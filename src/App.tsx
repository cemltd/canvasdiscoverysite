import { useEffect, useState, useRef } from "react";
import { setupAnalytics } from "./helpers/Analytics";
import { getAnalytics, logEvent } from "firebase/analytics";
import useWindowSize from "react-use/lib/useWindowSize";
import Confetti from "react-confetti";
import "animate.css";

import acclogo from "./assets/acclogo.png";
import canvasLogo from "./assets/logo.png";
import notebook from "./assets/notebook.png";
import pencilcase from "./assets/pencilcase.png";

import buttonBgParentObserver from "./assets/parent-observer.png";
import buttonBgTestScaffold from "./assets/scaffold.png";
import buttonBgExternal from "./assets/external.png";
import buttonBgStudent from "./assets/student.png";

import { version } from "./autobuild_version.js";

function App() {
  const { width, height } = useWindowSize();
  const [showConfetti, setShowConfetti] = useState(false);
  const [hiddenOptionsClickCount, setHiddenOptionsClickCount] = useState(0);
  const [showHiddenOptions, setShowHiddenOptions] = useState(false);
  const hiddenOptionsButtonRef = useRef<HTMLImageElement>(null);

  const showGoogleLogin = false;
  const showSamlLogin = true;
  const showCanvasLogin = true;

  useEffect(() => {
    setupAnalytics();
  }, []);

  useEffect(() => {
    const max = 15;
    if (hiddenOptionsClickCount <= max) {
      if (hiddenOptionsClickCount === 5) {
        console.log("Getting there, you can do it. " + hiddenOptionsClickCount);
      }
      if (hiddenOptionsClickCount === 10) {
        console.log("Woah, its almost ready." + hiddenOptionsClickCount);
      }
      if (hiddenOptionsClickCount >= max) {
        console.log("Go crazy. Beta and Test enabled.");
        const analytics = getAnalytics();
        logEvent(analytics, "select_content", {
          content_type: "betatest_buttons_enabled",
        });
        setShowHiddenOptions(true);
        setShowConfetti(true);
        removeHiddenOptionsButton();
      }
    }
  }, [hiddenOptionsClickCount]);

  function removeHiddenOptionsButton() {
    hiddenOptionsButtonRef.current?.remove();
  }

  function handleHiddenOptionsClick() {
    console.log("Tap");
    setHiddenOptionsClickCount((prevTapCount) => prevTapCount + 1);
  }

  function handleButtonClickedEvent(button_name: string) {
    const analytics = getAnalytics();
    logEvent(analytics, "select_content", {
      content_type: button_name,
    });
  }

  return (
    <>
      {showConfetti && <Confetti width={width} height={height} />}
      <h1>
        <img className="acclogo" alt="" src={acclogo} />
        <br />
        <img className="canvaslogo" alt="" src={canvasLogo} />
        <br />
        <br />
        <span>Choose a Canvas Login Type:</span>
      </h1>

      <div className="images-container">
        <img
          alt=""
          className="righthandimage unselectable-image"
          src={notebook}
        />
        <img
          ref={hiddenOptionsButtonRef}
          alt=""
          className="lefthandimage unselectable-image"
          src={pencilcase}
          onClick={handleHiddenOptionsClick}
        />
      </div>

      <div className="links-container">
        <a
          className="student-staff"
          onClick={() => {
            handleButtonClickedEvent("student_staff_login");
          }}
          href="https://learning.acc.edu.au/login/saml"
          style={{
            backgroundSize: "cover",
            background: `#fff  url(${buttonBgStudent}) left top`,
          }}
        >
          <span className="co-icn">
            <i className="fa fa-graduation-cap"></i>
          </span>
          <span className="co-text">
            Student/Staff <span className="co-text-tag">Login</span>
          </span>
        </a>

        <a
          className="parent-observer"
          onClick={() => {
            handleButtonClickedEvent("parent_observer_login");
          }}
          href="https://learning.acc.edu.au/login/canvas"
          style={{
            backgroundSize: "cover",
            background: `#fff  url(${buttonBgParentObserver}) left top`,
          }}
        >
          <span className="co-icn">
            <i className="fa fa-users"></i>
          </span>
          <span className="co-text">
            Parent/Observer <span className="co-text-tag">Login</span>
          </span>
        </a>

        <a
          className="external"
          onClick={() => {
            handleButtonClickedEvent("external_login");
          }}
          href="https://learning.acc.edu.au/login/canvas"
          style={{
            backgroundSize: "cover",
            background: `#fff  url(${buttonBgExternal}) left top`,
          }}
        >
          <span className="co-icn">
            <i className="fas fa-school"></i>
          </span>
          <span className="co-text">
            External
            <span className="co-text-tag">Login</span>
          </span>
        </a>

        {showHiddenOptions && (
          <>
            {showGoogleLogin && (
              <>
                <a
                  className="betatest"
                  onClick={() => {
                    handleButtonClickedEvent("test_instance_google_login");
                  }}
                  href="https://accde.test.instructure.com/login/google"
                  style={{
                    backgroundSize: "cover",
                    background: `#fff  url(${buttonBgTestScaffold}) left top`,
                  }}
                >
                  <span className="co-icn">
                    <i className="fas fa-code"></i>
                  </span>
                  <span className="co-text">
                    Test Instance (google)
                    <span className="co-text-tag">Login</span>
                  </span>
                </a>

                <a
                  className="betatest"
                  onClick={() => {
                    handleButtonClickedEvent("beta_instance_google_login");
                  }}
                  href="https://accde.beta.instructure.com/login/google"
                  style={{
                    backgroundSize: "cover",
                    background: `#fff  url(${buttonBgTestScaffold}) left top`,
                  }}
                >
                  <span className="co-icn">
                    <i className="fas fa-object-ungroup"></i>
                  </span>
                  <span className="co-text">
                    Beta Instance (google)
                    <span className="co-text-tag">Login</span>
                  </span>
                </a>
              </>
            )}

            {showSamlLogin && (
              <>
                <a
                  className="betatest"
                  onClick={() => {
                    handleButtonClickedEvent("test_instance_saml_login");
                  }}
                  href="https://accde.test.instructure.com/login/saml"
                  style={{
                    backgroundSize: "cover",
                    background: `#fff  url(${buttonBgTestScaffold}) left top`,
                  }}
                >
                  <span className="co-icn">
                    <i className="fas fa-code"></i>
                  </span>
                  <span className="co-text">
                    Test Instance (SAML)
                    <span className="co-text-tag">Login</span>
                  </span>
                </a>

                <a
                  className="betatest"
                  onClick={() => {
                    handleButtonClickedEvent("beta_instance_saml_login");
                  }}
                  href="https://accde.beta.instructure.com/login/saml"
                  style={{
                    backgroundSize: "cover",
                    background: `#fff  url(${buttonBgTestScaffold}) left top`,
                  }}
                >
                  <span className="co-icn">
                    <i className="fas fa-object-ungroup"></i>
                  </span>
                  <span className="co-text">
                    Beta Instance (SAML)
                    <span className="co-text-tag">Login</span>
                  </span>
                </a>
              </>
            )}

            {showCanvasLogin && (
              <>
                <a
                  className="betatest"
                  onClick={() => {
                    handleButtonClickedEvent("test_instance_canvas_login");
                  }}
                  href="https://accde.test.instructure.com/login/canvas"
                  style={{
                    backgroundSize: "cover",
                    background: `#fff  url(${buttonBgTestScaffold}) left top`,
                  }}
                >
                  <span className="co-icn">
                    <i className="fas fa-code"></i>
                  </span>
                  <span className="co-text">
                    Test Instance (canvas)
                    <span className="co-text-tag">Login</span>
                  </span>
                </a>

                <a
                  className="betatest"
                  onClick={() => {
                    handleButtonClickedEvent("test_instance_canvas_login");
                  }}
                  href="https://accde.beta.instructure.com/login/canvas"
                  style={{
                    backgroundSize: "cover",
                    background: `#fff  url(${buttonBgTestScaffold}) left top`,
                  }}
                >
                  <span className="co-icn">
                    <i className="fas fa-object-ungroup"></i>
                  </span>
                  <span className="co-text">
                    Beta Instance (canvas)
                    <span className="co-text-tag">Login</span>
                  </span>
                </a>
              </>
            )}

            <div
              style={{
                color: "lightgrey",
                font: "small-caption",
              }}
            >
              {"v:" + version}
            </div>
          </>
        )}
      </div>
    </>
  );
}

export default App;
