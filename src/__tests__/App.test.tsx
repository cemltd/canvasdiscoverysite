import { describe, it, expect, vi } from "vitest";
import "@testing-library/jest-dom";
import { render, act } from "@testing-library/react";
import App from "../App";

describe("App Test Suite", () => {
  it("Renders the main page", () => {
    render(<App />);
    expect(true).toBeTruthy();
  });

  it("Clicking the left hand image (class lefthandimage) should show the buttons with betatest className to appear", () => {
    const { container } = render(<App />);

    expect(container.getElementsByClassName("betatest")[0]).toBeUndefined();

    // Replace Confetti with a mock function that does nothing to prevent it from being called during tests
    vi.mock("react-confetti", () => ({ __esModule: true, default: vi.fn() }));

    const lefthandimage = container.getElementsByClassName(
      "lefthandimage"
    )[0] as HTMLImageElement;
    act(() => {
      for (let i = 0; i < 15; i++) {
        lefthandimage.click();
      }
    });

    expect(container.getElementsByClassName("betatest")[0]).toBeInTheDocument();
  });

  it("Displays a Student Login, Parent Login and External Login links", () => {
    const { container } = render(<App />);
    expect(container.getElementsByClassName("student-staff")[0]).toBeVisible();
    expect(
      container.getElementsByClassName("parent-observer")[0]
    ).toBeVisible();
    expect(container.getElementsByClassName("external")[0]).toBeVisible();
  });
});
