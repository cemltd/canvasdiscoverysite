const animateCSS = (
  element: HTMLElement,
  animationParams: string[],
  prefix = "animate__"
) =>
  // We create a Promise and return it
  new Promise((resolve) => {
    const node = element;

    for (const animation of animationParams) {
      const animationName = `${prefix}${animation}`;
      node.classList.add(`${prefix}animated`, animationName);
    }

    // When the animation ends, we clean the classes and resolve the Promise
    function handleAnimationEnd(event: Event) {
      event.stopPropagation();
      for (const animation of animationParams) {
        const animationName = `${prefix}${animation}`;
        node.classList.remove(`${prefix}animated`, animationName);
      }
      resolve("Animation ended");
    }

    node.addEventListener("animationend", handleAnimationEnd, { once: true });
  });

export default animateCSS;
