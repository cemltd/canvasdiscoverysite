import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

export function setupAnalytics() {
  // Analytics\
  let analytics = null;
  try {
    const ENV = import.meta.env;
    const firebaseConfig: {
      [key: string]: string;
    } = {
      apiKey: ENV.VITE_apiKey as string,
      authDomain: ENV.VITE_authDomain as string,
      projectId: ENV.VITE_projectId as string,
      storageBucket: ENV.VITE_storageBucket as string,
      messagingSenderId: ENV.VITE_messagingSenderId as string,
      appId: ENV.VITE_appId as string,
      measurementId: ENV.VITE_measurementId as string,
    };
    // Initialize Firebase
    const firebaseApp = initializeApp(firebaseConfig);
    analytics = getAnalytics(firebaseApp);
  } catch (e) {
    console.log("Analytics could not be loaded");
  }

  return analytics;
}
