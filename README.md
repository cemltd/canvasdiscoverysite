# Canvas Discovery Site

The repository of source code for the Canvas discovery site encompasses a login discovery website designed to facilitate seamless user experience for staff, parents, students, and external users alike. This platform seamlessly integrates with the Learning Management System (LMS) Canvas, offering easy login capabilities through various authentication methods including email addresses, usernames, and unique account identifiers.

In addition to core functionalities, the site offers customizable settings allowing administrators to tailor features according to specific educational institution requirements. The source code repository is meticulously organized for easy maintenance, modifications, and future expansion, making it an adaptable solution for diverse learning environments.

Other examples of live canvas discovery websites

- https://canvas.cccd.edu/
- https://www.callaghancollege.com.au/canvas/
- https://stsred.lincolnedu.com/
- https://canvas.utt.edu.tt/
- https://anzsog.edu.au/canvas-discovery-page/

## React + TypeScript + Vite

Vite React Typescript with HMR and some ESLint rules.
Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

# To Set the Gcloud project

`gcloud config set project PROJECT_ID`

Add If necessary

```json
    "deploy:live": "gcloud storage rsync -p -r ./dist/ gs://www.acc.edu.au/canvas/",
    "deploy:backup": "gcloud storage rsync -p -r ./backup-site/ gs://www.acc.edu.au/canvas/",
    "deploy:test": "gcloud storage rsync -p -r ./dist/ gs://www.acc.edu.au/canvas/test/"
```

Remember to add .env file with the following variables

```shell
VITE_apiKey=
VITE_authDomain=
VITE_projectId=
VITE_storageBucket=
VITE_messagingSenderId=
VITE_appId=
VITE_measurementId=
```

#### If don't need to deploy Add [skip ci] to your git commit message

## Expanding the ESLint configuration

If you are developing a production application, we recommend updating the configuration to enable type aware lint rules:

- Configure the top-level `parserOptions` property like this:

```js
export default {
  // other rules...
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
    project: ["./tsconfig.json", "./tsconfig.node.json"],
    tsconfigRootDir: __dirname,
  },
};
```

# Statistics

To view statistics visit the firebase google analytics project:
https://console.firebase.google.com/project/canvas-landing-page/overview

## Create a new branch and submit a pull request for changes. [Ref](https://cemschools.atlassian.net/wiki/spaces/ISD/pages/1558380546/Collaborative+Development+Using+Git)
